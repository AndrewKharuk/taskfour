﻿using System;

namespace TaskFour
{
    class Program
    {
        static void Main(string[] args)
        {
            CircularLinkedList<string> list = new CircularLinkedList<string>();

            list.Add("One");
            list.Add("Two");
            list.Add("Three");
            list.Add("Four");
            list.Add("Five");
            
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
